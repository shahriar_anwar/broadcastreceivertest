package com.example.u.test;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.u.test.app.Config;
import com.example.u.test.util.StringUtils;

public class MainActivity extends AppCompatActivity {

    TextView tvIncomingSummary, tvBatteryLevel;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        showSummary();
    }

    private void init() {
        preferences = getSharedPreferences(Config.PREF_NAME, MODE_PRIVATE);

        tvIncomingSummary = (TextView) findViewById(R.id.tvIncomingSummary);
        tvBatteryLevel = (TextView) findViewById(R.id.tvBatteryLevel);
    }

    private void showSummary() {
        showIncomingSummary();
        showBatteryUsage();
    }

    private void showIncomingSummary() {
        int incomingCount = preferences.getInt(Config.PREF_KEY_INCOMING_COUNT, 0);
        String incomingSummary = incomingCount + " " + StringUtils.pluralize("incoming", incomingCount);
        tvIncomingSummary.setText(incomingSummary);
    }

    private void showBatteryUsage() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, filter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

        String batteryLevelSummary = level + "%";
        tvBatteryLevel.setText(batteryLevelSummary);
    }

}
