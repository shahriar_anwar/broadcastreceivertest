package com.example.u.test.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.example.u.test.app.Config;

public class CallStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (action.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            SharedPreferences preferences = context.getSharedPreferences(Config.PREF_NAME, Context.MODE_PRIVATE);

            int previousCount = preferences.getInt(Config.PREF_KEY_INCOMING_COUNT, 0);
            previousCount = previousCount + 1;

            preferences.edit().putInt(Config.PREF_KEY_INCOMING_COUNT, previousCount).commit();
        }
    }
}
