package com.example.u.test.util;

/**
 * Created by U on 12/17/2015.
 */
public class StringUtils {
    public static String pluralize(String text, int count) {
        //todo: apply all plural logic
        return count > 1 ? text + "s" : text;
    }
}
